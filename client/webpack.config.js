'use strict'

const path = require('path')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const __environment__ = process.env.NODE_ENV || 'development'

module.exports = {
    devtool: '#eval-source-map',

    entry: './ts/main.ts',

    output: {
        path: path.join(__dirname, '../static/js/'),
        filename: 'app.js'
    },

    module: {
        rules: [
            /*
      {
        enforce: 'pre',
        test: /\.ts$/,
        loader: 'tslint-loader',
        exclude: /node_modules/
      },
      */
            {
                test: /\.ts$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
                options: {
                    appendTsSuffixTo: [/\.vue$/],
                }
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        ts: 'ts-loader',
                        scss: ExtractTextPlugin.extract('css-loader!sass-loader'),
                        sass: 'vue-style-loader!css-loader!sass-loader?indentedSyntax',
                        css: ExtractTextPlugin.extract('css-loader'),
                    },
                    esModule: true,
                    extractCSS: true,
                }
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract('css-loader!sass-loader')
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('css-loader')
            },
        ]
    },

    resolve: {
        extensions: ['.ts', '.js'],
        alias: {
            'vue$': 'vue/dist/vue.common.js'
        }
    },

    performance: {
        hints: false,
    },

    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(__environment__),
        }),
        new ExtractTextPlugin({
            filename: '../css/style.css',
            allChunks: true,
        }),
    ]
}

if (__environment__ === 'production') {
  module.exports.devtool = '#source-map'
    /*
  module.exports.plugins.push(new webpack.optimize.UglifyJsPlugin({
    compress: {
      warnings: false,
    },
  }))
  */
  module.exports.plugins.push(new webpack.LoaderOptionsPlugin({
    minimize: true,
  }))
}
