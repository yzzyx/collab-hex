export class Project {
    uuid: string
    filename: string

    constructor (uuid: string, filename: string) {
        this.uuid = uuid;
        this.filename = filename;
    }
}
