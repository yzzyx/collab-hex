export class Mark {
    static idCounter = 1;
    id : number;

    constructor(public start = 0,
        public end = 0,
        public color = "color-yellow") {
        this.id = Mark.idCounter++;
    }

    markUpdated () {
        for (let i = this.start; i <= this.end; i++) {
            // Remove previous colors, if set
            let h = document.getElementById("h-" + i);
            let c = document.getElementById("c-" + i);

            h.className = h.className.replace(/color-\w*/,"") + " " + this.color;
            c.className = c.className.replace(/color-\w*/,"") + " " + this.color;

            h.classList.add('mark');
            c.classList.add('mark');
            h.classList.add(`mark-${this.id}`);
            c.classList.add(`mark-${this.id}`);
        }
    }
}
