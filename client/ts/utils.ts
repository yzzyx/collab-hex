import long from 'long';
/*
/// <reference path="./imports/debounce.d.ts" />
import * as debounceFunction from 'debounce';

// debounce decorator
export function debounce(timeout: number) {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        debounceFunction(target, timeout);
    }
}
*/

/*
 * decodeValues expects a list of values, and will return
 * string representations for all types it can decode the list as
 */
export function decodeValues(s:Uint8Array) : {[key:string]:string} {
    let decodedValue : {[key:string]:string} = {};

    if (s.length <= 8) {
        let bitstr = "";
        for (let i = 0; i < s.length; i ++) {
            for (let mask = 1; mask <= 8; mask ++) {
                bitstr += (s[i] & 1 << mask) ? '1' : '0';
            }
        }
        decodedValue['bit'] = bitstr;
    }

    if (s.length == 2) {
        let be = (s[0] << 8 | s[1]);
        let le = (s[1] << 8 | s[0]);
        decodedValue['int16be'] = `0x${le.toString(16)} (${le})`;
        decodedValue['int16le'] = `0x${be.toString(16)} (${be})`;
    } else if (s.length == 4) {
        // Since all shift operations except >>> work on signed 32bit integers in JS,
        // we need to stuff things around to avoid ending up with negative numbers
        let be = [s[0] << 8 | s[1], s[2] << 8 | s[3]];
        let le = [s[3] << 8 | s[2], s[1] << 8 | s[0]];
        decodedValue['int32be'] = `${le[0].toString(16)}${le[1].toString(16)} (${((le[0] << 16)>>>0) + le[1]})`;
        decodedValue['int32le'] = `${be[0].toString(16)}${be[1].toString(16)} (${((be[0] << 16)>>>0) + be[1]})`;
    } else if (s.length == 8) {
        // For 64 bit values, we need to use long.js

        // Do some ancient black magic with our bits to get both the LE and BE versions
        let arr = decodedValue['bit'].split("");
        let reversed = arr.map((e:any,i:number) => {return (i%8===0)?arr.slice(i,i+8):null;}) // split into groups of 8bits
                            .filter((e:any) => { return e !== null})  // remove null values created along the way
                            .reverse()                                // reverse order of groups
                            .map((e:any) => {return e.join("");})     // convert group back to strings
                            .join("");                                // join all groups back to one group


        let be = long.fromString(decodedValue['bit'], true, 2);
        let le = long.fromString(reversed, true, 2);
        decodedValue['int64be'] = `${le.toString(16)} (${le.toString()})`;
        decodedValue['int64le'] = `${be.toString(16)} (${be.toString()})`;
    }

    return decodedValue;
}

