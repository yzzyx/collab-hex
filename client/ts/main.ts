import * as Vue from 'vue';
import {CreateElement, VNode} from 'vue';
import request from 'superagent';
import Vuex from 'vuex'
import {ActionContext} from 'vuex'
import VueRouter from 'vue-router'
import {Route, RouteRecord, RedirectOption} from 'vue-router'
import {Mark} from './mark/mark.ts'

Vue.use(VueRouter)
Vue.use(Vuex)

class State {
    data : Uint8Array;

    marks : Array<Mark>
    markCurrentIdx : number
    markSelection : number[];

    constructor () {
        this.data = new Uint8Array([]);

        this.marks = [];
        this.markCurrentIdx = -1;
        this.markSelection = [-1, -1];
    }
}

var store = new Vuex.Store({
  state: new State(),
  mutations: {
      /* Disabled for now - no inserts allowed
       * handling of inserts is harder - the number of
       * rows might need to be updated, etc. etc.
      insertData(state : State, params : { position: number, val: number }) {
          state.data.splice(params.position, 0, params.val);
      },
      */
      changeData(state : State, params :  { position: number, val: number }) {
          // We cannot use Vue.set - it tries to convert data to Array<any>
          // and call splice, which is not allowed on Uint8Arrays (they're fixed length)
          state.data[params.position] = params.val & 0xff;
          (<any>state.data).__ob__.dep.notify();
      },

      setData(state : State, data: Uint8Array) {
          state.data = data;
      },

      markSelection(state : State, params : { start: number, end : number }) {
          state.markSelection = [params.start, params.end];
          state.markCurrentIdx = -1;
      },

      markSetColor(state : State, color : string) {
          let mark : Mark;
          if (state.markCurrentIdx == -1) {
              mark = new Mark(state.markSelection[0], state.markSelection[1], color)
              let pos = state.marks.length;
              state.marks.push(mark);
              state.markCurrentIdx = pos;
          } else {
              mark = state.marks[state.markCurrentIdx];
          }
          mark.color = color;
          mark.markUpdated();
      },
  },
  getters: {
      getData: (state : State, getters : any) => (start: number, end :number) => {
          return state.data.slice(start, end);
      },

      markGetCurrent: (state: State, getters : any) => () => {
          if (state.markCurrentIdx == -1)
              return null;
          return state.marks[state.markCurrentIdx];
      },

      // markGetClass(pos)
      //
      // Returns mark classes set for data[pos]
      markGetClass: (state: State, getters : any) => (position: number) => {
          return "";
      }

  },
  actions: {
  },
});

var hexMain = require('./hex.main.vue').default;
var startPage = require('./start.vue').default;
const config = {
    linkActiveClass: 'active',
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes: [
        { name: 'start', path: '/', component: startPage },
        { name: 'hexeditor', path: '/:uuid/hex', component: hexMain },
    ]
}

var app = require('./app.vue').default;
var router = new VueRouter(config)
new Vue({
  el: '#app',
  components: { app },
  store: store,
  router: router,
  render: function(createElement : CreateElement) : VNode {
      return createElement(app);
  },
  mounted: function () {
    //this.$store.dispatch('loadDelegationRoles')
  }
});
