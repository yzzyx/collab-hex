package main

import (
	"fmt"

	"github.com/gorilla/websocket"
)

type Client struct {
	hub    *Hub
	send   chan []byte
	socket *websocket.Conn
}

func (c *Client) Writer() {
	defer func() {
		c.hub.unregister <- c
		c.socket.Close()
	}()

	for {
		select {
		// Message on queu
		case msg, ok := <-c.send:

			// send-channel is closed
			if !ok {
				c.socket.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			err := c.socket.WriteMessage(websocket.BinaryMessage, msg)
			if err != nil {
				fmt.Println(err)
				c.hub.unregister <- c
				return
			}
		}
	}
}

func (c *Client) SendHello() error {
	err := c.socket.WriteMessage(websocket.BinaryMessage, []byte{0x00})
	if err != nil {
		c.socket.Close()
		return err
	}
	return nil
}

func (c *Client) Reader() {
	defer func() {
		c.hub.unregister <- c
		c.socket.Close()
	}()

	for {
		msgType, msg, err := c.socket.ReadMessage()
		if err != nil {
			fmt.Println(err)
			break
		}

		if msgType == websocket.TextMessage {
			fmt.Printf("Recieved a textmessage, expected binary message")
			c.hub.unregister <- c
			c.socket.Close()
			break
		}

		c.hub.broadcast <- Message{from: c, data: msg}
	}
}
