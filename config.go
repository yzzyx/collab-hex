package main

type Config struct {
	IpAddress string `toml:ipadress`
	Port      int    `toml:port`
}
