package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/websocket"
)

type Message struct {
	from *Client
	data []byte
}

type Hub struct {
	clients    map[*Client]bool
	broadcast  chan Message
	register   chan *Client
	unregister chan *Client
}

func NewHub() *Hub {
	return &Hub{
		clients:    make(map[*Client]bool),
		broadcast:  make(chan Message),
		register:   make(chan *Client),
		unregister: make(chan *Client),
	}
}

func (h *Hub) Run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok == true {
				delete(h.clients, client)
				close(client.send)
			}
		case message := <-h.broadcast:
			for client := range h.clients {
				// Skip client it was sent from
				if client == message.from {
					continue
				}

				// Broadcast message
				select {
				case client.send <- message.data:
				default:
					// Could not write to chan
					close(client.send)
					delete(h.clients, client)
				}
			}
		}
	}
}

func (h *Hub) Handler(w http.ResponseWriter, r *http.Request) {
	var upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}

	socket, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println(err)
		return
	}

	client := &Client{
		hub:    h,
		socket: socket,
		send:   make(chan []byte, 10),
	}

	// Send hello message
	err = client.SendHello()
	if err != nil {
		return
	}

	client.hub.register <- client
	go client.Writer()
	client.Reader()
}
