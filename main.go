package main

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"

	"github.com/BurntSushi/toml"
	"github.com/Sirupsen/logrus"
	"github.com/pressly/chi"
	"github.com/pressly/chi/middleware"
)

var logger *logrus.Logger

const configPath = "collab-hex.toml"

var logfile string = "asset.log"
var config Config

func main() {

	_, err := os.Stat(configPath)
	if err != nil {
		panic(fmt.Sprintf("Cannot access config file %s", configPath))
	}

	if _, err := toml.DecodeFile(configPath, &config); err != nil {
		panic(fmt.Sprintf("Cannot read config file %s: %s", configPath, err.Error()))
	}

	ctrl := Controller{&InMemoryBackend{}}
	hub := NewHub()

	r := chi.NewRouter()

	r.Use(middleware.RequestID) // Assign unique ID to request
	r.Use(middleware.Logger)    // Add logging to stdout
	//r.Use(middleware.Recoverer)           // Recover from panics
	r.Use(middleware.DefaultCompress) // Assign unique ID to request
	r.Get("/api/websocket", hub.Handler)
	r.Get("/api/project/", ctrl.projectsListHandler)
	r.Post("/api/project/", ctrl.projectsUploadHandler)
	r.Get("/api/project/:uuid/", ctrl.projectsGetHandler)

	clientPath := filepath.Join(".", "static")
	r.FileServer("/", http.Dir(clientPath))

	bindAddr := fmt.Sprintf("%s:%d", config.IpAddress, config.Port)
	fmt.Printf("Starting webserver on http://%s\n", bindAddr)

	// Start hub
	go hub.Run()

	server := http.Server{Addr: bindAddr, Handler: r}
	err = server.ListenAndServe()
	if err != nil {
		fmt.Printf("Error occurred: %s", err)
	}
}
