package main

/*
Database layout


projects
--------
project_id uuid
filename   string
datafile   string
owner      -> ou

ou
-------
id     int
name   string
passwd string
email  string
type   person/organisation


ou_members
----------
ou_parent -> ou
ou_member -> ou


file_comments
---------
project_id -> projects
tag_start  int
tag_end    int
created_by -> ou
color      int
comment    string


*/

// Models
type Project struct {
	UUID string `json:"uuid"`
	Name string `json:"name"`

	// Non-exported fields fields
	id       int    `json:"id"`
	filename string `json:"filename"`
}

type ProjectFilter struct {
	UUID string
}

type DatabaseBackend interface {
	ProjectAdd(project *Project) error
	ProjectList(filter ProjectFilter) ([]Project, error)
}

type InMemoryBackend struct {
	projects []Project
}

func (db *InMemoryBackend) ProjectList(filter ProjectFilter) ([]Project, error) {
	projectlist := []Project{}

	for _, v := range db.projects {
		if filter.UUID != "" && v.UUID != filter.UUID {
			continue
		}
		projectlist = append(projectlist, v)
	}
	return projectlist, nil
}

func (db *InMemoryBackend) ProjectAdd(project *Project) error {
	project.id = len(db.projects) + 1
	db.projects = append(db.projects, *project)
	return nil
}
