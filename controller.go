package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"github.com/pressly/chi"
	uuid "github.com/satori/go.uuid"
)

type Controller struct {
	db DatabaseBackend
}

func (c *Controller) projectsListHandler(w http.ResponseWriter, r *http.Request) {

	projects, err := c.db.ProjectList(ProjectFilter{})
	if err != nil {
		fmt.Printf("Cannot read projectlist: %s\n", err.Error())
		http.Error(w, err.Error(), 500)
		return
	}

	data, err := json.Marshal(projects)
	if err != nil {
		fmt.Printf("JSON marshal error: %s\n", err.Error())
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Write(data)
}

func (c *Controller) projectsUploadHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseMultipartForm(32 << 20)
	file, header, err := r.FormFile("file")
	if err != nil {
		fmt.Printf("Upload file error: %s\n", err.Error())
		http.Error(w, err.Error(), 500)
		return
	}
	defer file.Close()

	fmt.Printf("Uploaded filename: %s\n", header.Filename)
	tmpfile, err := ioutil.TempFile("files", "")
	if err != nil {
		fmt.Printf("Could not create temporary file: %s\n", err.Error())
		http.Error(w, err.Error(), 500)
		return
	}
	defer tmpfile.Close()

	// Copy complete contents of file
	io.Copy(tmpfile, file)

	uuid := uuid.NewV4()
	filename := tmpfile.Name()

	type Response struct {
		Status  int     `json:"status"`
		Project Project `json:"project"`
	}

	project := Project{UUID: uuid.String(),
		Name:     header.Filename,
		filename: filename,
	}

	err = c.db.ProjectAdd(&project)
	if err != nil {
		fmt.Printf("ProjectAdd error: %s\n", err.Error())
		http.Error(w, err.Error(), 500)
		return
	}

	data, err := json.Marshal(Response{Status: 0, Project: project})
	if err != nil {
		fmt.Printf("JSON marshal error: %s\n", err.Error())
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Write(data)
}

func (c *Controller) projectsGetHandler(w http.ResponseWriter, r *http.Request) {
	uuid := chi.URLParam(r, "uuid")

	projects, err := c.db.ProjectList(ProjectFilter{UUID: uuid})
	if err != nil {
		fmt.Printf("ProjectList error: %s\n", err.Error())
		http.Error(w, err.Error(), 500)
		return
	}

	if len(projects) == 0 {
		http.Error(w, "No such project", 404)
		return
	}

	filedata, err := ioutil.ReadFile(projects[0].filename)
	if err != nil {
		fmt.Printf("ReadFile error: %s\n", err.Error())
		http.Error(w, err.Error(), 500)
		return
	}

	type Response struct {
		Project
		Data []byte `json:"data"`
	}

	resp := Response{projects[0], filedata}

	data, err := json.Marshal(resp)
	if err != nil {
		fmt.Printf("JSON marshal error: %s\n", err.Error())
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Write(data)
}
